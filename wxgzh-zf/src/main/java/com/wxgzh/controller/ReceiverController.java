package com.wxgzh.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@RestController
public class ReceiverController {

    // 临时目录路径
    private static final String TEMP_DIR = System.getProperty("java.io.tmpdir");

    @PostMapping("/download-json")
    public ResponseEntity<String> downloadJson(@RequestBody String jsonMessage) {
        // 创建目标文件路径
        Path targetFilePath = Paths.get(TEMP_DIR, "downloadedJson.json");

        try {
            // 将输入流中的数据写入文件
            Files.write(targetFilePath, jsonMessage.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);

            // 返回成功消息
            return ResponseEntity.ok("JSON file downloaded successfully.");
        } catch (IOException e) {
            // 处理异常
            return ResponseEntity.status(500).body("Failed to download JSON file: " + e.getMessage());
        }
    }
}