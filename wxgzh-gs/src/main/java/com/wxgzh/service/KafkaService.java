package com.wxgzh.service;

public interface KafkaService {
    public void kafkaProduceJsonFile();
    public void kafkaConsumeJsonFile();
}
