package com.wxgzh.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JsonUtil {


    //



    //获取当天创建的json文件
    private static File findJsonFileCreatedToday(String directoryPath) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String today = sdf.format(new Date());
        try (Stream<Path> paths = Files.walk(Paths.get(directoryPath))) {
            List<Path> jsonFiles = paths
                    .filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".json"))
                    .filter(path -> {
                        BasicFileAttributes attrs = null;
                        try {
                            attrs = Files.readAttributes(path, BasicFileAttributes.class);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        return sdf.format(attrs.creationTime().toMillis()).equals(today);
                    })
                    .collect(Collectors.toList());
            if (!jsonFiles.isEmpty()) {
                return jsonFiles.get(0).toFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
